package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author kafun
 * @email 513112374@qq.com
 * @date 2021-10-10 18:11:11
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
