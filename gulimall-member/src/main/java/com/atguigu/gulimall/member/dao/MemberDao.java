package com.atguigu.gulimall.member.dao;

import com.atguigu.gulimall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author kafun
 * @email 513112374@qq.com
 * @date 2021-10-11 00:29:26
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
