package com.atguigu.gulimall.coupon.dao;

import com.atguigu.gulimall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author kafun
 * @email 513112374@qq.com
 * @date 2021-10-11 00:07:25
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
