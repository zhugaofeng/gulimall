package com.atguigu.gulimall.order.dao;

import com.atguigu.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author kafun
 * @email 513112374@qq.com
 * @date 2021-10-11 00:41:44
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
