package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author kafun
 * @email 513112374@qq.com
 * @date 2021-10-11 00:46:08
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
